import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class FactorialTest {
    public void factorialTest() {
        Assert.assertEquals(MathFunctions.factorial(3),6,"Числа не равны");
        Assert.assertEquals(MathFunctions.factorial(12),479001600,"Числа не равны");
        Assert.assertEquals(MathFunctions.factorial(3),5, "Числа не равны");
    }
}
