import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TxtFileParser {
    /*
    * Преобразование содержимого файла в строку*/
    public String readFile() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src\\main\\resources\\test.txt"));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }
    /*
    * Преобразование строки в массив целых чисел*/
    public int[] convertStringToArray(String string) {
        String[] str = string.split(",");
        int[] numbers=new int[str.length];
        for (int i=0;i<str.length;i++) {
            numbers[i] = Integer.parseInt(str[i]);
        }
        return numbers;
    }
    /*
    * Вывод массива в консоль*/
    public void printArray(int[] arr) {
        for (int i=0;i<arr.length;i++) {
            System.out.print(arr[i]+" ");
        }
    }
    /*
    * Сортировка массива по восходящей*/
    void ascendingSort(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int t = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = t;
                }
            }
        }
        System.out.println("Ascending sort: ");
        printArray(arr);
    }
    /*
     * Сортировка массива по низходящей*/
    void descendingSort(int[] arr) {
        for (int i=arr.length-1;i>0;i--) {
            for (int j=0;j<i;j++) {
                if (arr[j] < arr[j + 1]) {
                    int t = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = t;
                }
            }
        }
        System.out.println("Descending sort: ");
        printArray(arr);
    }

//    public int[] ascendingSort

    public static void main(String args[]) throws IOException {
        TxtFileParser txtFileParser = new TxtFileParser();
        String strings=txtFileParser.readFile();
        System.out.println("Parsed file:\n"+strings);
        int[] numbers=txtFileParser.convertStringToArray(strings);
        txtFileParser.descendingSort(numbers);
        System.out.println(" ");
        txtFileParser.ascendingSort(numbers);

    }
}
