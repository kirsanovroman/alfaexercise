public class MathFunctions {
    /*
    * Рассчет факториала*/
    public static  int factorial(int n) {
            int fact=1;
            for (int i=1;i<=n;i++) {
                fact *= i;
            }
            return fact;
        }


        public static void main(String[] args) {
            MathFunctions mathFunctions = new MathFunctions();
            System.out.println(mathFunctions.factorial(20));
        }

}
